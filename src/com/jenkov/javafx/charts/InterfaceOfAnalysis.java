package com.jenkov.javafx.charts;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.Point;
import javax.swing.JPanel;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Font;

public class InterfaceOfAnalysis {

	private JFrame frmAnalysis;
	
	//private JFrame

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceOfAnalysis window = new InterfaceOfAnalysis();
					window.frmAnalysis.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfaceOfAnalysis() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frmAnalysis = new JFrame();
		frmAnalysis.setTitle("Analysis");
		frmAnalysis.setResizable(false);
		frmAnalysis.getContentPane().setBackground(Color.WHITE);
		frmAnalysis.getContentPane().setLayout(null);
		
		JButton btnElectricityBill = new JButton("Electricity Bill");
		btnElectricityBill.setFont(new Font("Tahoma", Font.BOLD, 36));
		btnElectricityBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				ElectricityBillChart elec = new ElectricityBillChart();
//				elec.NewscreenElectricity();
				
				//BillChart cha = new BillChart();
				//cha.NewscreenChart();
//				frmAnalysis.dispose();
//				InterfaceOfAnalysis window = new InterfaceOfAnalysis();
//				window.frmAnalysis.setVisible(true);
				
			}
		});
		btnElectricityBill.setForeground(Color.WHITE);
		btnElectricityBill.setBackground(Color.BLACK);
		btnElectricityBill.setBounds(108, 78, 291, 55);
		frmAnalysis.getContentPane().add(btnElectricityBill);
		
		JButton btnWaterBill = new JButton("Water Bill");
		btnWaterBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WaterBillChart wat = new WaterBillChart();
				wat.NewscreenWater();
			}
		});
		btnWaterBill.setForeground(Color.WHITE);
		btnWaterBill.setFont(new Font("Tahoma", Font.BOLD, 36));
		btnWaterBill.setBackground(Color.BLACK);
		btnWaterBill.setBounds(108, 199, 291, 55);
		frmAnalysis.getContentPane().add(btnWaterBill);
		
		JButton btnPhoneBill = new JButton("Phone Bill");
		btnPhoneBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PhoneBillChart ph = new PhoneBillChart();
				ph.NewscreenPhone();
			}
		});
		btnPhoneBill.setForeground(Color.WHITE);
		btnPhoneBill.setFont(new Font("Tahoma", Font.BOLD, 36));
		btnPhoneBill.setBackground(Color.BLACK);
		btnPhoneBill.setBounds(108, 342, 291, 55);
		frmAnalysis.getContentPane().add(btnPhoneBill);
		
		JButton btnBack = new JButton("");
		btnBack.setIcon(new ImageIcon(InterfaceOfAnalysis.class.getResource("/com/jenkov/javafx/charts/Back.jpg")));
		btnBack.setBounds(795, 469, 28, 30);
		frmAnalysis.getContentPane().add(btnBack);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setOpaque(true);
		lblNewLabel.setIcon(new ImageIcon(InterfaceOfAnalysis.class.getResource("/com/jenkov/javafx/charts/analysis tab.jpg")));
		lblNewLabel.setBounds(-13, 0, 887, 539);
		frmAnalysis.getContentPane().add(lblNewLabel);
		frmAnalysis.setBounds(100, 100, 875, 566);
		frmAnalysis.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}