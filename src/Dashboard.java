

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;


import javax.swing.SwingConstants;
import javax.swing.Timer;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;





public class Dashboard extends JFrame {



	private JPanel contentPane;
	private JLabel lblNewLabel_7;



	
	
	public Dashboard () {

		
		setTitle("Billing Management System(Domestic purpose only)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 1024);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1185,750);
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		JLabel lblLogSuc = new JLabel("Login successful....");
		lblLogSuc.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 13));
		lblLogSuc.setForeground(new Color(255, 255, 0));
		lblLogSuc.setBounds(829, 30, 0, 25);
		panel.add(lblLogSuc);
		
		Timer labelRight = new Timer (100,new ActionListener() {
			
		public void actionPerformed(ActionEvent e) {
				
				
				if(lblLogSuc.getWidth() != 0) {
					lblLogSuc.setBounds(lblLogSuc.getX(), lblLogSuc.getY(), lblLogSuc.getWidth()-5, lblLogSuc.getHeight());
					}
				else {
					((Timer)e.getSource()).stop();
				}
			}
				
		});
		
		
		Timer labelLeft = new Timer (30,new ActionListener() {
	
			public void actionPerformed(ActionEvent e) {
		
				if(lblLogSuc.getWidth() !=160) {
					lblLogSuc.setBounds(lblLogSuc.getX(), lblLogSuc.getY(), lblLogSuc.getWidth()+5, lblLogSuc.getHeight());
					}
				else {
					((Timer)e.getSource()).stop();
				}
			}
	
		});	
		

		

		labelLeft.start();
		
		labelRight.start();

		

		

		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon("Images\\okbtn.png"));
		lblNewLabel_3.setBounds(320, 138, 57, 57);
		panel.add(lblNewLabel_3);
		
		lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblNewLabel_7.setVisible(false);
				lblNewLabel_3.setVisible(true);
				
				
			}
		});
		lblNewLabel_7.setIcon(new ImageIcon("Images\\cross.png"));
		lblNewLabel_7.setBounds(320, 138, 57, 57);
		panel.add(lblNewLabel_7);
		
		JLabel lblNewLabel_6 = new JLabel("+Telephone");
		lblNewLabel_6.setFont(new Font("Teko SemiBold", Font.BOLD, 50));
		lblNewLabel_6.setForeground(Color.YELLOW);
		lblNewLabel_6.setBounds(676, 516, 233, 57);
		panel.add(lblNewLabel_6);
		lblNewLabel_6.setVisible(false);
		
		JLabel lblNewLabel_5 = new JLabel("+Water");
		lblNewLabel_5.setFont(new Font("Teko SemiBold", Font.BOLD, 50));
		lblNewLabel_5.setForeground(Color.YELLOW);
		lblNewLabel_5.setBounds(676, 391, 256, 82);
		panel.add(lblNewLabel_5);
		lblNewLabel_5.setVisible(false);
		
		JLabel lblNewLabel_4 = new JLabel("+Electricity");
		lblNewLabel_4.setForeground(Color.YELLOW);
		lblNewLabel_4.setFont(new Font("Teko SemiBold", Font.BOLD, 50));
		lblNewLabel_4.setBounds(676, 277, 256, 82);
		panel.add(lblNewLabel_4);
		lblNewLabel_4.setVisible(false);
		
;
		
		JButton btnNewButton_5_1 = new JButton("Add Paid Bill Details");
		btnNewButton_5_1.setBounds(603, 360, 374, 105);
		panel.add(btnNewButton_5_1);
		btnNewButton_5_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel_7.setVisible(true);
				lblNewLabel_3.setVisible(false);
				
				lblNewLabel_4.setVisible(true);
				lblNewLabel_5.setVisible(true);
				lblNewLabel_6.setVisible(true);
				btnNewButton_5_1.setVisible(false);
			}
			
		});
		btnNewButton_5_1.setIcon(new ImageIcon("Images\\addbtn.png"));
		btnNewButton_5_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		

		
		JButton btnNewButton = new JButton("Status");
		btnNewButton.setForeground(new Color(153, 255, 255));
		btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton.setFont(new Font("Quicksand", Font.BOLD, 40));
		btnNewButton.setBackground(new Color(0, 0, 51));
		btnNewButton.setBounds(59, 138, 256, 57);
		panel.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Check status button");
			}
		});
		
		JButton btnNewButton_4_1 = new JButton("Analysis");
		btnNewButton_4_1.setForeground(new Color(153, 255, 255));
		btnNewButton_4_1.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_4_1.setFont(new Font("Quicksand", Font.BOLD, 40));
		btnNewButton_4_1.setBackground(new Color(0, 0, 51));
		btnNewButton_4_1.setBounds(59, 627, 318, 57);
		panel.add(btnNewButton_4_1);
		
		JButton btnNewButton_4 = new JButton("Bill Statement");
		btnNewButton_4.setForeground(new Color(153, 255, 255));
		btnNewButton_4.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_4.setFont(new Font("Quicksand", Font.BOLD, 40));
		btnNewButton_4.setBackground(new Color(0, 0, 51));
		btnNewButton_4.setBounds(59, 529, 318, 57);
		panel.add(btnNewButton_4);
		
		JButton btnNewButton_3 = new JButton("Telephone Bill");
		btnNewButton_3.setForeground(new Color(153, 255, 255));
		btnNewButton_3.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_3.setFont(new Font("Quicksand", Font.BOLD, 40));
		btnNewButton_3.setBackground(new Color(0, 0, 51));
		btnNewButton_3.setBounds(59, 427, 318, 57);
		panel.add(btnNewButton_3);
		
		JButton btnNewButton_2 = new JButton("Water Bill");
		btnNewButton_2.setForeground(new Color(153, 255, 255));
		btnNewButton_2.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_2.setFont(new Font("Quicksand", Font.BOLD, 40));
		btnNewButton_2.setBackground(new Color(0, 0, 51));
		btnNewButton_2.setBounds(59, 326, 318, 57);
		panel.add(btnNewButton_2);
		
		JButton btnNewButton_1 = new JButton("Electricity Bill");
		btnNewButton_1.setForeground(new Color(153, 255, 255));
		btnNewButton_1.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_1.setFont(new Font("Quicksand", Font.BOLD, 40));
		btnNewButton_1.setBackground(new Color(0, 0, 51));
		btnNewButton_1.setBounds(59, 231, 318, 57);
		panel.add(btnNewButton_1);
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 204, 255));
		panel_1.setBounds(420, 113, 4, 609);
		panel.add(panel_1);
		
		JLabel lblProfile = new JLabel("");
		lblProfile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				LoginInterface frame = new LoginInterface();
				frame.show();
				dispose();
			}
		});
		lblProfile.setToolTipText("logout");
		lblProfile.setIcon(new ImageIcon("Images\\profile1.png"));
		lblProfile.setBounds(1092, 0, 93, 97);
		panel.add(lblProfile);
		
		JLabel lblNewLabel_1 = new JLabel("\r\nDashboard\r\n");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Playball", Font.BOLD, 30));
		lblNewLabel_1.setBounds(20, 30, 186, 57);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("Add Paid Bill \r\nDetails");
		lblNewLabel.setForeground(Color.YELLOW);
		lblNewLabel.setIcon(new ImageIcon("Images\\bg_dash.png"));
		lblNewLabel.setBounds(0, 0, 1185,750);
		panel.add(lblNewLabel);
		

		

		


		

	}

}
